package id.co.nexsoft.productapp.controller;

import id.co.nexsoft.productapp.entity.Brand;
import id.co.nexsoft.productapp.repository.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BrandController {

    @Autowired
    private BrandRepository brandRepository;

    @GetMapping("/")
    public String welcome() {
        return "<html><body>"
                + "<h1>WELCOME</h1>"
                + "</body></html>";
    }

    @GetMapping("/brand")
    public List<Brand> getAllNotes() {
        return brandRepository.findAll();
    }

    @GetMapping("/brand/{id}")
    public Brand getStudentById(@PathVariable(value = "id") int id) {
        return brandRepository.findById(id);
    }

    @PostMapping("/brand")
    @ResponseStatus(HttpStatus.CREATED)
    public Brand addStudent(@RequestBody Brand student) {
        return brandRepository.save(student);
    }
}
