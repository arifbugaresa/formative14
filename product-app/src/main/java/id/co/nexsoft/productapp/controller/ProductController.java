package id.co.nexsoft.productapp.controller;

import id.co.nexsoft.productapp.entity.Product;
import id.co.nexsoft.productapp.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/product")
    public List<Product> getAllNotes() {
        return productRepository.findAll();
    }

    @GetMapping("/product/{id}")
    public Product getStudentById(@PathVariable(value = "id") int id) {
        return productRepository.findById(id);
    }
}
